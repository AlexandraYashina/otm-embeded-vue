// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import './styles/main.scss'
import 'mapbox-gl/dist/mapbox-gl.css';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faChevronDown, faChevronUp, faBusAlt, faStoreAlt, faMugHot } from '@fortawesome/free-solid-svg-icons'

library.add(faChevronDown, faChevronUp, faBusAlt, faStoreAlt, faMugHot)
 
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
