import Vue from 'vue'

class CategSet {
    constructor () {
        this.state = new Vue({
            data: {
                dictionary: {
                    table: {},
                    nElements: 0
                }
            }
        })
    }

    clear () {
        this.state.dictionary.table = {};
        this.state.dictionary.nElements = 0;
    }

    size () {
        return this.state.dictionary.nElements;
    }

    isEmpty () {
        return this.state.dictionary.nElements <= 0;
    }
    toString () {
        var str = '',
            name;
        for (name in this.state.dictionary.table) {
            if (this.state.dictionary.table.hasOwnProperty(name)) {
                str += name + ",";
            }
        }
        return str.length == 0 ? '' : str.substring(0, str.length-1);
    }
        fromTable (val) {
            var cat;
             for (cat in  val) {
                if (this.state.dictionary.table[cat] === undefined) {
                    this.state.dictionary.nElements += 1;
                }
                this.state.dictionary.table[cat] = true;
            }
        }
        fromString(str) {
            var arr = [];
            if( Object.prototype.toString.call( str ) === '[object Array]' ) {
                arr = str;
            }
            else{
                arr = str.split(',');
            }
            for (var i = 0; i < arr.length; i++) {
                var key = arr[i].toString().trim();
                if (key == '')
                    continue;
                if (this.state.dictionary.table[key] === undefined) {
                    this.state.dictionary.nElements += 1;
                }
                this.state.dictionary.table[key] = true;
            }
        }
        fromStats(stats) {
            for (var prop in stats) {
                this.state.dictionary.table[prop] = true;
                this.state.dictionary.nElements++;
            }
        }
        getIntersection(otherSet, filter) {
            var array = [];
            for (var prop in this.state.dictionary.table) {
                if (otherSet.contains(prop) && filter[prop] !== undefined) {
                    array.push(prop);
                }
            }
            return array;
        }

        remove(key) {
            if (this.state.dictionary.table[key] !== undefined) {
                delete this.state.dictionary.table[key];
                this.state.dictionary.nElements -= 1;
            }
        }

        add(key) {
            if (key === undefined)
                return;
            key = '' + key;
            if (this.state.dictionary.table[key] === undefined) {
                this.state.dictionary.nElements += 1;
            }
            this.state.dictionary.table[key] = true;
        }

        contains(element) {
            if (this.state.dictionary.table[element] === undefined)
                return false;
            return true;
        }

        smartContains(element) {
            if (this.state.dictionary.table[element] === undefined) {
                for (var prop in this.state.dictionary.table) {
                    if (isCategorsParent(+prop, +element)) {
                        return true;
                    }
                }
                return false;
            }
            return true;
        }

        haveIntersection(otherSet) {
            if (this.size() < otherSet.size()) {
                for (var prop in this.state.dictionary.table) {
                    if (otherSet.contains(prop)) {
                        return true;
                    }
                }
            } else {
                for (var otherProp in otherSet.table) {
                    if (this.contains(otherProp)) {
                        return true;
                    }
                }
            }
            return false;
        }

        toArray() {
            var array = [],
                name;
            for (name in this.state.dictionary.table) {
                if (this.state.dictionary.table.hasOwnProperty(name)) {
                    array.push(name >> 0);
                }
            }
            return array;
        }
}

export default CategSet