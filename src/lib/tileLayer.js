import {throttle} from "./utils";
const EventEmitter = require('events');

export default class TileLayer extends EventEmitter {

	constructor(tags) {
		super();
		this.hash = "";
		this.tags = tags;
		this.onChangeExtent = this.onChangeExtent.bind(this);
		let self = this;
		map.on('moveend', () => {
			self.onChangeExtent();
        });
		this.onSourceData = this.onSourceData.bind(this);
		map.on('sourcedata', this.onSourceData);
		map.on('resize', () => {
			self.onChangeExtent();
		});
	}
	onSourceData(e) {
		if (e.isSourceLoaded && e.sourceId != "directions") {
			this.updateList(this.getHashString(), true);
		}
	}
	onChangeExtent() {
		throttle(this.updateList(this.getHashString(), false), 1000, {
			trailing: true
		});
	}
	updateList(cause, isForced) {
		if (this.hash != cause || isForced) {
			this.hash = cause;
			this.emit('changed', {});
		}
	}
	getHashString() {
		const center = map.getCenter(),
			zoom = Math.round(map.getZoom() * 100) / 100,
			// derived from equation: 512px * 2^z / 360 / 10^d < 0.5px
			precision = Math.ceil((zoom * Math.LN2 + Math.log(512 / 360 / 0.5)) / Math.LN10),
			m = Math.pow(10, precision),
			lng = Math.round(center.lng * m) / m,
			lat = Math.round(center.lat * m) / m,
			bearing = map.getBearing(),
			pitch = map.getPitch();
		let hash = '';
		hash += `#${zoom}/${lat}/${lng}`;
		if (bearing || pitch) hash += (`/${Math.round(bearing * 10) / 10}`);
		if (pitch) hash += (`/${Math.round(pitch)}`);
		return hash;
	}
};
