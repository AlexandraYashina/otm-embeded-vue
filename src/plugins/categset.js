import CategSet from '../lib/categset'

const categset = {
  install (Vue) {
    Vue.prototype.$categset = new CategSet()
  }
}

export default categset
