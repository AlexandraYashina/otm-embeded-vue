import TileLayer from '../lib/tileLayer'

const tilelayer = {
  install (Vue) {
    Vue.prototype.$tylelayer = new TileLayer()
  }
}

export default tilelayer
